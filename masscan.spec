Name:           masscan
Version:        1.3.2
Release:        1
Summary:        This is an Internet-scale port scanner

License:        AGPL-3.0
URL:            https://github.com/robertdavidgraham/masscan
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  make
BuildRequires:  clang
BuildRequires:  libpcap-devel

Requires:       libpcap-devel

%description
This is an Internet-scale port scanner. It can scan the entire 
Internet in under 6 minutes, transmitting 10 million packets 
per second, from a single machine.
It is a faster port scan that produces results similar to nmap,
the most famous port scanner. Internally, it operates more like
scanrand, unicornscan, and ZMap, using asynchronous transmission.

%prep
%autosetup
sed -i -e 's/CC =/CC ?=/g' Makefile
sed -i 's/\r$//' VULNINFO.md

%build
export CC=clang
%make_build

%install
%make_install
mkdir -p %{buildroot}%{_bindir}/
install -pm 0755 bin/masscan %{buildroot}%{_bindir}/%{name}
install -Dp -m 0644 doc/%{name}.8 %{buildroot}%{_mandir}/man8/%{name}.8

%files
%license LICENSE
%doc VULNINFO.md README.md
%{_bindir}/%{name}
%{_mandir}/man8/%{name}.*

%changelog
* Fri Mar 1 2024 wangqiang <wangqiang1@kylinos.cn> - 1.3.2-1
- package init
